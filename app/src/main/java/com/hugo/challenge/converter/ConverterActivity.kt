package com.hugo.challenge.converter

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.core.widget.doOnTextChanged
import androidx.databinding.DataBindingUtil
import com.hugo.challenge.converter.databinding.ActivityConverterBinding
import kotlin.math.roundToInt

class ConverterActivity : AppCompatActivity() {
    
    private lateinit var binding: ActivityConverterBinding
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val model: ConverterViewModel by viewModels()
        binding = DataBindingUtil.setContentView(this, R.layout.activity_converter)
        setContentView(binding.root)

        val versionName = "v${BuildConfig.VERSION_NAME}"
        binding.textViewVersion.text = versionName
        
        model.let { binding.viewModel = it }
        binding.lifecycleOwner = this

        loadFeature(model)
    }
    
    private fun loadFeature(model: ConverterViewModel) {
        binding.textInputEditTextCelsius.doOnTextChanged { celsius, _, _, _ ->
            if (binding.textInputEditTextFahrenheit.isFocused.not()) {
                if (celsius.isNullOrEmpty().not()) {
                    model.fahrenheit.set(toFahrenheit(celsius.toString().toDouble()).toString())
                } else {
                    model.fahrenheit.set(0.toString())
                }
            }
        }
        binding.textInputEditTextFahrenheit.doOnTextChanged { fahrenheit, _, _, _ ->
            if (binding.textInputEditTextCelsius.isFocused.not()) {
                if (fahrenheit.isNullOrEmpty().not()) {
                    model.celsius.set(toCelsius(fahrenheit.toString().toDouble()).toString())
                } else {
                    model.celsius.set(0.toString())
                }
            }
        }

        binding.buttonReset.setOnClickListener {
            if (binding.textInputEditTextCelsius.text.isNullOrEmpty().not() &&
                binding.textInputEditTextFahrenheit.text.isNullOrEmpty().not()) {
                binding.textInputEditTextFahrenheit.text?.clear()
                binding.textInputEditTextCelsius.text?.clear()
            }
        }
    }

    // to fahrenheit -> F = C * (9/5) + 32.
    private fun toFahrenheit(celsius: Double): Int {
        val factor = celsius * 9/5
        return (factor + 32).roundToInt()
    }

    // to celsius is -> C = (F - 32) * (5/9)
    private fun toCelsius(fahrenheit: Double): Int {
        val factor = fahrenheit - 32
        return (factor * 5/9).roundToInt()
    }


}