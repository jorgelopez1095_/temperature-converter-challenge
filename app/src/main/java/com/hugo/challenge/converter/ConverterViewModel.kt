package com.hugo.challenge.converter

import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel

class ConverterViewModel: ViewModel() {
    var fahrenheit: ObservableField<String> = ObservableField()
    var celsius: ObservableField<String> = ObservableField()
}