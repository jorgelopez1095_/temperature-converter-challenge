package com.hugo.challenge.converter

import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter

@BindingAdapter("validateTextField")
fun bindValidateTextField(textView: TextView, value: String?){
    if (value.isNullOrEmpty().not()) {
        if (value.toString().toInt() < 0) {
            textView.setTextColor(
                ContextCompat.getColor(textView.context, R.color.colorAlizarin)
            )
        } else {
            textView.setTextColor(
                ContextCompat.getColor(textView.context, R.color.colorMidnightBlue)
            )
        }
    }
}
