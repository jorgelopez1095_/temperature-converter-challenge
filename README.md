# C O N V E R T E R | challenge

Challenges: bidirectional data flow, user-provided text input.
The task is to build a frame containing two textfields TC and TF representing the temperature in Celsius and Fahrenheit, respectively. Initially, both TC and TF are empty. When the user enters a numerical value into TC the corresponding value in TF is automatically updated and vice versa. When the user enters a non-numerical string into TC the value in TF is not updated and vice versa. The formula for converting a temperature C in Celsius into a temperature F in Fahrenheit is C = (F - 32) * (5/9) and the dual direction is F = C * (9/5) + 32.
[DOWNLOAD APK](https://drive.google.com/file/d/1wjUk-3eugA6nJ8BEA3S-HP4ZmuQwwsq7/view?usp=sharing)

### SCREENSHOTS
-----------

![init-view](screenshots/converter_i.jpeg "Main-view application")

### LANGUAGES, LIBRARIES AND TOOLS USED

* [Kotlin](https://kotlinlang.org/)
* [Android Architecture Components](https://developer.android.com/topic/libraries/architecture/index.html)
* [DataBinding](https://developer.android.com/topic/libraries/view-binding?hl=es-419)
* [Arch Lifecycle](https://developer.android.com/reference/android/arch/lifecycle/package-summary)
* [ConstraintLayouts](https://developer.android.com/training/constraint-layout)

### REQUIREMENTS

* JDK 1.8
* [Android SDK](https://developer.android.com/studio/index.html)
* [Kotlin Version](https://kotlinlang.org/docs/releases.html)
* [MIN SDK 23](https://developer.android.com/preview/api-overview.html))
* [Build Tools Version 30.0.3](https://developer.android.com/studio/releases/build-tools)
* Latest Android SDK Tools and build tools.

### ARCHITECTURE

- N/A Just activity and view.

## L I C E N S E

Under the [MIT license](https://opensource.org/licenses/MIT).

